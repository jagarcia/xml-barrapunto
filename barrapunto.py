#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

enlace = ""
class myContentHandler(ContentHandler):

    def __init__ (self):
        self.inItem = False
        self.inContent = False
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.inContent = True

    def endElement (self, name):
        if name == 'item':
            self.inItem = False
            fichero.write( "<p><a href='" + self.link + "'>" + self.theContent1 + "</a></p>")
        elif self.inItem:
            if name == 'title':
                self.theContent1 = self.theContent
                self.inContent = False
                self.theContent =""
            elif name == 'link':
                self.link = self.theContent
                self.inContent = False
                self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog


theParser = make_parser()
theHandler = myContentHandler()
theParser.setContentHandler(theHandler)

# Ready, set, go!
url = "http://barrapunto.com/barrapunto.rss"
fichero = open("doc.html",'w')
fichero.write("<html><head><h2> Titulares Barrapunto </h2><body><ul>")
xmlFile = urllib.request.urlopen(url)
theParser.parse(xmlFile)
fichero.write("</ul></body></head></html>")


